﻿using RPGGame.Attributes;
using RPGGame.Characters.Classes;
using RPGGame.Exceptions;
using RPGGame.ItemsAndEquipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace RPGGameTests
{
    public class EquipmentTests
    {
        [Fact]
        public void EquipItem_TooHighWeaponLevel_ThrowInvalidWeaponException()
        {
            // Arrange
            Warrior testWarrior = new Warrior("Test");
            Weapon testAxe = new Weapon("Common Axe", 2, EquipmentSlot.Weapon, WeaponType.Axe, new WeaponAttributes(7, 1.1));
            
            // Act
            Action actual = () => testWarrior.EquipItem(testAxe);

            // Assert
            Assert.Throws<InvalidWeaponException>(actual);
        }

        [Fact]
        public void EquipItem_TooHighArmorLevel_ThrowInvalidArmorException()
        {
            // Arrange
            Warrior testWarrior = new Warrior("Test");
            Armor testArmor = new Armor("Common Plate Armor", 2, EquipmentSlot.Body, ArmorType.Plate, new PrimaryAttributes(1,0,0));

            // Act
            Action actual = () => testWarrior.EquipItem(testArmor);

            // Assert
            Assert.Throws<InvalidArmorException>(actual);
        }

        [Fact]
        public void EquipItem_WrongWeaponType_ThrowInvalidWeaponException()
        {
            // Arrange
            Warrior testWarrior = new Warrior("Test");
            Weapon testBow = new Weapon("Common Bow", 2, EquipmentSlot.Weapon, WeaponType.Bow, new WeaponAttributes(12, 0.8));

            // Act
            Action actual = () => testWarrior.EquipItem(testBow);

            // Assert
            Assert.Throws<InvalidWeaponException>(actual);
        }

        [Fact]
        public void EquipItem_WrongArmorType_ThrowInvalidArmorException()
        {
            // Arrange
            Warrior testWarrior = new Warrior("Test");
            Armor testArmor = new Armor("Common Cloth head armor", 2, EquipmentSlot.Head, ArmorType.Cloth, new PrimaryAttributes(0, 0, 5));

            // Act
            Action actual = () => testWarrior.EquipItem(testArmor);

            // Assert
            Assert.Throws<InvalidArmorException>(actual);
        }

        [Fact]
        public void EquipItem_ValidWeaponEquipped_ReturnSuccessMessage()
        {
            // Arrange
            Warrior testWarrior = new Warrior("Test");
            Weapon testAxe = new Weapon("Common Axe", 1, EquipmentSlot.Weapon, WeaponType.Axe, new WeaponAttributes(12, 0.8));

            // Act
            string actual = testWarrior.EquipItem(testAxe);

            // Assert
            Assert.Equal("New weapon equipped!", actual);
        }

        [Fact]
        public void EquipItem_ValidArmorEquipped_ReturnSuccessMessage()
        {
            // Arrange
            Warrior testWarrior = new Warrior("Test");
            Armor testArmor = new Armor("Common Plate Armor", 1, EquipmentSlot.Body, ArmorType.Plate, new PrimaryAttributes(1, 0, 0));

            // Act
            string actual = testWarrior.EquipItem(testArmor);

            // Assert
            Assert.Equal("New armor equipped!", actual);
        }

        [Fact]
        public void DealDamage_CalculateDamageDoneNoWeapon_ExpectedDamage()
        {
            // Arrange
            Warrior testWarrior = new Warrior("Test");
            double expected = 1 * (1 + (5 / 100));

            // Act
            double actual = testWarrior.DealDamage();

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DealDamage_CalculateDamageDoneValidWeapon_ExpectedDamage()
        {
            // Arrange
            Warrior testWarrior = new Warrior("Test");
            Weapon testAxe = new Weapon("Common Axe", 1, EquipmentSlot.Weapon, WeaponType.Axe, new WeaponAttributes(7, 1.1));

            testWarrior.EquipItem(testAxe);

            double expected = (7 * 1.1) * (1 + (5 / 100));

            // Act
            double actual = testWarrior.DealDamage();

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DealDamage_CalculateDamageDoneValidWeaponAndArmor_ExpectedDamage()
        {
            // Arrange
            Warrior testWarrior = new Warrior("Test");
            Armor testArmor = new Armor("Common Plate Armor", 1, EquipmentSlot.Body, ArmorType.Plate, new PrimaryAttributes(1, 0, 0));
            Weapon testAxe = new Weapon("Common Axe", 1, EquipmentSlot.Weapon, WeaponType.Axe, new WeaponAttributes(7, 1.1));

            testWarrior.EquipItem(testArmor);
            testWarrior.EquipItem(testAxe);

            double expected = (7 * 1.1) * (1 + ((5 + 1) / 100));

            // Act
            double actual = testWarrior.DealDamage();

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
