using RPGGame.Attributes;
using RPGGame.Characters.Classes;
using System;
using Xunit;

namespace RPGGameTests
{
    public class CharacterTests
    {
        [Fact]
        public void RangerConstructor_RangerLevelOnCreated_ShouldReturnOne()
        {
            // Arrange
            Ranger ranger = new Ranger("Test");
            int expected = 1;

            // Act
            int actual = ranger.Level;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_RangerOnLevelUp_ShouldReturnTwo()
        {
            // Arrange
            Ranger ranger = new Ranger("Test");
            int expected = 2;
           
            // Act
            ranger.LevelUp();
            int actual = ranger.Level;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MageConstructor_BaseAttributesIsSet_ShouldBeDefaultAttributes()
        {
            // Arrange
            Mage mage = new Mage("Test");
            PrimaryAttributes expected = new PrimaryAttributes(1,1,8);

            // Act
            PrimaryAttributes actual = mage.BaseAttributes;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RangerConstructor_BaseAttributesIsSet_ShouldBeDefaultAttributes()
        {
            // Arrange
            Ranger ranger = new Ranger("Test");
            PrimaryAttributes expected = new PrimaryAttributes(1, 7, 1);

            // Act
            PrimaryAttributes actual = ranger.BaseAttributes;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RougeConstructor_BaseAttributesIsSet_ShouldBeDefaultAttributes()
        {
            // Arrange
            Rogue rogue = new Rogue("Test");
            PrimaryAttributes expected = new PrimaryAttributes(2, 6, 1);

            // Act
            PrimaryAttributes actual = rogue.BaseAttributes;

            // Assert
            Assert.Equal(expected, actual);
        }


        [Fact]
        public void WarriorConstructor_BaseAttributesIsSet_ShouldBeDefaultAttributes()
        {
            // Arrange
            Warrior warrior = new Warrior("Test");
            PrimaryAttributes expected = new PrimaryAttributes(5, 2, 1);

            // Act
            PrimaryAttributes actual = warrior.BaseAttributes;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_MageBaseAttributesShouldChange_IncreaseOfBaseAttributesBasedOnClass()
        {
            // Arrange
            Mage mage = new Mage("Test");
            PrimaryAttributes expected = mage.BaseAttributes + new PrimaryAttributes(1,1,5);

            // Act
            mage.LevelUp();
            PrimaryAttributes actual = mage.BaseAttributes;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_RangerBaseAttributesShouldChange_IncreaseOfBaseAttributesBasedOnClass()
        {
            // Arrange
            Ranger ranger = new Ranger("Test");
            PrimaryAttributes expected = ranger.BaseAttributes + new PrimaryAttributes(1, 5, 1);

            // Act
            ranger.LevelUp();
            PrimaryAttributes actual = ranger.BaseAttributes;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_RougeBaseAttributesShouldChange_IncreaseOfBaseAttributesBasedOnClass()
        {
            // Arrange
            Rogue rogue = new Rogue("Test");
            PrimaryAttributes expected = rogue.BaseAttributes + new PrimaryAttributes(1, 4, 1);

            // Act
            rogue.LevelUp();
            PrimaryAttributes actual = rogue.BaseAttributes;

            // Assert
            Assert.Equal(expected, actual);
        }


        [Fact]
        public void LevelUp_WarriorBaseAttributesShouldChange_IncreaseOfBaseAttributesBasedOnClass()
        {
            // Arrange
            Warrior warrior = new Warrior("Test");
            PrimaryAttributes expected = warrior.BaseAttributes + new PrimaryAttributes(3, 2, 1);

            // Act
            warrior.LevelUp();
            PrimaryAttributes actual = warrior.BaseAttributes;

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
