﻿using RPGGame.Attributes;
using RPGGame.Exceptions;
using RPGGame.ItemsAndEquipment;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGGame.Characters
{
    public abstract class Character
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public PrimaryAttributes BaseAttributes { get; set; }
        public Dictionary<EquipmentSlot, Equipment> EquippedItems { get; set; }
        public WeaponType[] EquippableWeapons { get; set; }
        public ArmorType[] EquippableArmors { get; set; }

        /// <summary>
        /// Constructor for Character, where it requires a name and initalizes the EquippedItems and sets starter level to 1.
        /// </summary>
        /// <param name="name">The name of the character</param>
        public Character(string name)
        {
            Name = name;
            EquippedItems = new Dictionary<EquipmentSlot, Equipment>();
            Level = 1;
        }

        /// <summary>
        /// Increases respective baseattribute stats in their respective classes.
        /// </summary>
        protected abstract void IncreaseStatsOnLevelUp();

        /// <summary>
        /// Gets the main attribute value of the respective class, for example strength for warrior.
        /// </summary>
        /// <returns>The main attribute value, for example strength for warrior</returns>
        protected abstract int GetMainAttributeValue();

        /// <summary>
        /// The damage method which calculates the damage done
        /// </summary>
        /// <returns>The damage that is done</returns>
        public double DealDamage()
        {
            double damage;

            // Gets the value of the main attribute
            int mainAttribute = GetMainAttributeValue();

            //Checks if there is a weapon equipped
            if (!EquippedItems.ContainsKey(EquipmentSlot.Weapon))
            {
                damage = 1 * (1 + mainAttribute / 100);
            }
            else // If no weapon equipped
            {
                Weapon weapon = (Weapon)EquippedItems[EquipmentSlot.Weapon];
                damage = weapon.CalculateWeaponDPS() * (1 + mainAttribute / 100);
            }
            return damage;
        }

        /// <summary>
        /// Increases level by one and increases baseattribute stats in their respective classes
        /// </summary>
        public void LevelUp()
        {
            Level++;
            IncreaseStatsOnLevelUp();
        }

        /// <summary>
        /// Equips a weapon, throws an exception if it cannot be equipped, or gives out a successful message of equipping.
        /// </summary>
        /// <param name="equipment">The equipment passed in, in this case a weapon</param>
        /// <returns>The successful message of equipping the message</returns>
        /// <exception cref="InvalidWeaponException">Throws if the weaponType is wrong, level is too high or it doesn't fit in the slot</exception>
        public string EquipItem(Weapon equipment)
        {
            string message = string.Empty;
            if (!EquippableWeapons.Contains(equipment.WeaponType) || Level < equipment.LevelRequired || equipment.EquipmentSlot != EquipmentSlot.Weapon)
            {
                throw new InvalidWeaponException();
            }
            message = "New weapon equipped!";
            EquippedItems[equipment.EquipmentSlot] = equipment;
            return message;
        }

        /// <summary>
        /// Equips an armor, throws an exception if it cannot be equipped, or gives out a successful message of equipping.
        /// </summary>
        /// <param name="equipment">The equipment passed in, in this case an armor</param>
        /// <returns>The successful message of equipping the message</returns>
        /// <exception cref="InvalidArmorException">Throws if the armorType is wrong, level is too high or it doesn't fit in the slot</exception>
        public string EquipItem(Armor equipment)
        {
            string message = string.Empty;
            if(!EquippableArmors.Contains(equipment.ArmorType) || Level < equipment.LevelRequired || equipment.EquipmentSlot == EquipmentSlot.Weapon)
            {
                throw new InvalidArmorException();
            }
            message = "New armor equipped!";
            EquippedItems[equipment.EquipmentSlot] = equipment;
            return message;
        }

        /// <summary>
        /// Calculates the total attributes of the character (Base and armor)
        /// </summary>
        /// <returns>The total attributes calculated, base + all armor</returns>
        protected PrimaryAttributes CalculateTotalAttributes()
        {
            PrimaryAttributes totalAttributes = new PrimaryAttributes();
            totalAttributes += BaseAttributes; // Adding base attributes

            foreach (Equipment equipment in EquippedItems.Values) // Loops through all the items
            {
                if (equipment is Armor armor) // If the equipment is an Armor and set it as such
                {
                    totalAttributes += armor.Attributes; // Add the armor attributes to the total
                }
            }

            return totalAttributes;
        }

        /// <summary>
        /// Prints out a character stats screen with use of StringBuilder to generate string
        /// </summary>
        public void PrintCharacterStats()
        {
            PrimaryAttributes totalAttributes = CalculateTotalAttributes();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine($"Character Name: {Name}");
            stringBuilder.AppendLine($"Level: {Level}");
            stringBuilder.AppendLine($"Strength: {totalAttributes.Strength}");
            stringBuilder.AppendLine($"Dexterity: {totalAttributes.Dexterity}");
            stringBuilder.AppendLine($"Intelligence: {totalAttributes.Intelligence}");
            stringBuilder.AppendFormat("Damage dealt {0:0.00}", DealDamage());
            Console.WriteLine(stringBuilder.ToString());
        }
    }
}
