﻿using RPGGame.Attributes;
using RPGGame.ItemsAndEquipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGGame.Characters.Classes
{
    public class Rogue : Character
    {
        /// <summary>
        /// Constructor for creating the character Rogue, setting the equippable armor/weapon and baseattributes.
        /// </summary>
        /// <param name="name">Setting the name of the character</param>
        public Rogue(string name) : base(name)
        {
            BaseAttributes = new PrimaryAttributes
            {
                Strength = 2,
                Dexterity = 6,
                Intelligence = 1,
            };

            EquippableArmors = new ArmorType[] { ArmorType.Leather, ArmorType.Mail };
            EquippableWeapons = new WeaponType[] { WeaponType.Dagger, WeaponType.Sword };
        }

        /// <summary>
        /// Gets the main attribute value to the character.
        /// </summary>
        /// <returns>The main attribute</returns>
        protected override int GetMainAttributeValue()
        {
            int dexterity;
            dexterity = CalculateTotalAttributes().Dexterity;
            return dexterity;
        }

        /// <summary>
        /// Increases the base attributes of the character.
        /// </summary>
        protected override void IncreaseStatsOnLevelUp()
        {
            BaseAttributes += new PrimaryAttributes(1, 4, 1);
        }
    }
}
