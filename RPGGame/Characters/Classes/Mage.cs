﻿using RPGGame.Attributes;
using RPGGame.ItemsAndEquipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGGame.Characters.Classes
{
    public class Mage : Character
    {
        /// <summary>
        /// Constructor for creating the character Mage, setting the equippable armor/weapon and baseattributes.
        /// </summary>
        /// <param name="name">Setting the name of the character</param>
        public Mage(string name) : base(name)
        {
            BaseAttributes = new PrimaryAttributes
            {
                Strength = 1,
                Dexterity = 1,
                Intelligence = 8,
            };

            EquippableArmors = new ArmorType[] { ArmorType.Cloth };
            EquippableWeapons = new WeaponType[] { WeaponType.Staff, WeaponType.Wand };
        }

        /// <summary>
        /// Gets the main attribute value to the character.
        /// </summary>
        /// <returns>The main attribute</returns>
        protected override int GetMainAttributeValue()
        {
            int intelligence;
            intelligence = CalculateTotalAttributes().Intelligence;
            return intelligence;
        }

        /// <summary>
        /// Increases the base attributes of the character.
        /// </summary>
        protected override void IncreaseStatsOnLevelUp()
        {
            BaseAttributes += new PrimaryAttributes(1, 1, 5);
        }
    }
}
