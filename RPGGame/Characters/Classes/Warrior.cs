﻿using RPGGame.Attributes;
using RPGGame.ItemsAndEquipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGGame.Characters.Classes
{
    public class Warrior : Character
    {
        /// <summary>
        /// Constructor for creating the character Warrior, setting the equippable armor/weapon and baseattributes.
        /// </summary>
        /// <param name="name">Setting the name of the character</param>
        public Warrior(string name) : base(name)
        {
            BaseAttributes = new PrimaryAttributes
            {
                Strength = 5,
                Dexterity = 2,
                Intelligence = 1,
            };

            EquippableArmors = new ArmorType[] { ArmorType.Mail, ArmorType.Plate };
            EquippableWeapons = new WeaponType[] { WeaponType.Axe, WeaponType.Hammer, WeaponType.Sword };
        }

        /// <summary>
        /// Gets the main attribute value to the character.
        /// </summary>
        /// <returns>The main attribute</returns>
        protected override int GetMainAttributeValue()
        {
            int strength;
            strength = CalculateTotalAttributes().Strength;
            return strength;
        }

        /// <summary>
        /// Increases the base attributes of the character.
        /// </summary>
        protected override void IncreaseStatsOnLevelUp()
        {
            BaseAttributes += new PrimaryAttributes(3,2,1);
        }
    }
}
