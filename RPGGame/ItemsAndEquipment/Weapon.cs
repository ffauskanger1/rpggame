﻿using RPGGame.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGGame.ItemsAndEquipment
{
    public class Weapon : Equipment
    {
        public WeaponType WeaponType { get; set; }
        public WeaponAttributes WeaponAttributes { get; set; }

        /// <summary>
        /// Constructor for Weapon, based on equipment
        /// </summary>
        /// <param name="name">The name of the weapon</param>
        /// <param name="levelRequired">The level required for the weapon</param>
        /// <param name="equipmentSlot">The equipmentslot of the weapon</param>
        /// <param name="weaponType">The weapon Type of the weapon, from the WeaponType enum</param>
        /// <param name="weaponAttributes">The weaponAttributes from WeaponAttributes</param>
        public Weapon(string name, int levelRequired, EquipmentSlot equipmentSlot, WeaponType weaponType, WeaponAttributes weaponAttributes) : base(name, levelRequired, equipmentSlot)
        {
            WeaponType = weaponType;
            WeaponAttributes = weaponAttributes;
        }

        /// <summary>
        /// Empty constructor for Weapon
        /// </summary>
        public Weapon()
        { }

        /// <summary>
        /// Calculating the Weapon DPS of a Weapon with DMG*Attackspeed
        /// </summary>
        /// <returns>The Weapon DPS</returns>
        public double CalculateWeaponDPS()
        {
            double weaponDPS;
            weaponDPS = WeaponAttributes.Damage * WeaponAttributes.AttackSpeed; 
            return weaponDPS;
        }
    }
    
    /// <summary>
    /// Weapontype enum for the different types of weapons
    /// </summary>
    public enum WeaponType
    {
        Axe,
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand,
    }
}
