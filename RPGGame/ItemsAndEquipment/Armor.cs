﻿using RPGGame.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGGame.ItemsAndEquipment
{
    public class Armor : Equipment
    {
        public ArmorType ArmorType { get; set; }
        public PrimaryAttributes Attributes { get; set; }

        /// <summary>
        /// Constructor for Armor, based on equipment
        /// </summary>
        /// <param name="name">Setting the name of the armor</param>
        /// <param name="levelRequired">Setting the level required for the armor</param>
        /// <param name="equipmentSlot">Setting the equipmentslot for the armor</param>
        /// <param name="armorType">Setting the armor type for the armor</param>
        /// <param name="attributes">Setting the primary attributes for the armor</param>
        public Armor(string name, int levelRequired, EquipmentSlot equipmentSlot, ArmorType armorType, PrimaryAttributes attributes) : base(name, levelRequired, equipmentSlot)
        {
            ArmorType = armorType;
            Attributes = attributes;
        }

        /// <summary>
        /// Empty constructor for armor
        /// </summary>
        public Armor()
        { }
    }

    /// <summary>
    /// Armortype enum for the different types of armors
    /// </summary>
    public enum ArmorType
    {
        Cloth,
        Leather,
        Mail,
        Plate,
    }
}
