﻿using RPGGame.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGGame.ItemsAndEquipment
{
    public abstract class Equipment
    {
        public string Name { get; set; }
        public int LevelRequired { get; set; }
        public EquipmentSlot EquipmentSlot { get; set; }

        /// <summary>
        /// Constructor for Equipment, where it requires name, levelRequired and equipmentslot
        /// </summary>
        /// <param name="name">The name of the equipment</param>
        /// <param name="levelRequired">The level that is required</param>
        /// <param name="equipmentSlot">The slot it's going to use</param>
        public Equipment(string name, int levelRequired, EquipmentSlot equipmentSlot)
        {
            Name = name;
            LevelRequired = levelRequired;
            EquipmentSlot = equipmentSlot;
        }

        /// <summary>
        /// Empty constructor for equipment
        /// </summary>
        public Equipment()
        { }
    }

    /// <summary>
    /// Equipmentslot enum for the different equipmentslots available
    /// </summary>
    public enum EquipmentSlot
    {
        Head,
        Body,
        Legs,
        Weapon,
    }
}
