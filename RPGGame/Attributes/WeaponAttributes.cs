﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGGame.Attributes
{
    public class WeaponAttributes
    {
        public int Damage { get; set; }
        public double AttackSpeed { get; set; }

        /// <summary>
        /// Constructor for WeaponAttributes
        /// </summary>
        /// <param name="damage">The damage of the weapon</param>
        /// <param name="attackSpeed">The attackspeed of the weapon</param>
        public WeaponAttributes(int damage = 0, double attackSpeed = 0)
        {
            Damage = damage;
            AttackSpeed = attackSpeed;
        }
        /// <summary>
        /// Empty constructor for WeaponAttributes
        /// </summary>
        public WeaponAttributes()
        { }
    }
}
