﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RPGGame.Attributes.PrimaryAttributes;

namespace RPGGame.Attributes
{
    public class PrimaryAttributes
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        /// <summary>
        /// Constructor for creating primary attributes
        /// </summary>
        /// <param name="strength">Setting strength</param>
        /// <param name="dexterity">Setting dexterity</param>
        /// <param name="intelligence">Setting intelligence</param>
        public PrimaryAttributes(int strength = 0, int dexterity = 0, int intelligence = 0)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }

        /// <summary>
        /// Empty constructor
        /// </summary>
        public PrimaryAttributes()
        { }

        /// <summary>
        /// Overriding the equals making it possible to compare values
        /// </summary>
        /// <param name="obj">The object of primaryattributes</param>
        /// <returns>True if it has the same values in the object, else false</returns>
        public override bool Equals(object obj)
        {
            return obj is PrimaryAttributes attributes &&
                   Strength == attributes.Strength &&
                   Dexterity == attributes.Dexterity &&
                   Intelligence == attributes.Intelligence;
        }

        /// <summary>
        /// Overriding the plus operator for addition of PrimaryAttributes 
        /// </summary>
        /// <param name="lhs">Left hand side</param>
        /// <param name="rhs">Right hand side</param>
        /// <returns>The new primaryattributes with summed values</returns>
        public static PrimaryAttributes operator +(PrimaryAttributes lhs, PrimaryAttributes rhs)
        {
            return new PrimaryAttributes
            {
                Strength = lhs.Strength + rhs.Strength,
                Dexterity = lhs.Dexterity + rhs.Dexterity,
                Intelligence = lhs.Intelligence + rhs.Intelligence
            };
        }
    }
}
