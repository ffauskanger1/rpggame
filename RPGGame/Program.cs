﻿using System;
using RPGGame.Attributes;
using RPGGame.Characters.Classes;
using RPGGame.ItemsAndEquipment;
using RPGGame.Characters;
using System.Collections.Generic;
using static System.Net.Mime.MediaTypeNames;

namespace RPGGame
{
    public class Program
    {
        static void Main(string[] args)
        {
            //Uncomment to simulate actions - This was added just for fun
            //StartGame();
        }

        static void StartGame()
        {
            Character character = GameSetup();
            bool gameLoop = true;

            while (gameLoop == true)
            {
                Console.WriteLine("Input number for action");
                Console.WriteLine("1: Levelup");
                Console.WriteLine("2. Print character stats");
                Console.WriteLine("3. Equip item");
                Console.WriteLine("4. Deal damage");
                Console.WriteLine("5. Quit");

                string input = Console.ReadLine();
                switch (input)
                {
                    case "1":
                        character.LevelUp();
                        break;
                    case "2":
                        character.PrintCharacterStats();
                        break;
                    case "3":
                        ItemsToEquip(character);
                        break;
                    case "4":
                        Console.WriteLine(String.Format("Damage dealt {0:0.00}", character.DealDamage()));
                        break;
                    case "5":
                        gameLoop = false;
                        break;
                    default:
                        Console.Write("Wrong input, try again: ");
                        break;
                }
            }
        }
        static Character GameSetup()
        {
            Console.Write("Welcome to the RPG Game, please input your name: ");

            string name = Console.ReadLine();
            Console.Write("Please choose a class between ranger, warrior, mage and rogue: ");

            Character c = null;

            while (c == null)
            {
                string hero = Console.ReadLine().ToLower();
                c = ChooseCharacter(name, hero);
            }

            return c;
        }
        static Character ChooseCharacter(string name, string typeOfCharacter)
        {
            switch (typeOfCharacter)
            {
                case "ranger":
                    return new Ranger(name);
                case "warrior":
                    return new Warrior(name);
                case "mage":
                    return new Mage(name);
                case "rogue":
                    return new Rogue(name);
                default:
                    Console.Write("Invalid character, try again: ");
                    break;
            }
            return null;
        }
        static void ItemsToEquip(Character character)
        {
            List<Equipment> equipmentList = new List<Equipment>();
            Weapon bow = new Weapon()
            {
                Name = "Common bow",
                LevelRequired = 1,
                EquipmentSlot = EquipmentSlot.Weapon,
                WeaponType = WeaponType.Bow,
                WeaponAttributes = new WeaponAttributes() { Damage = 12, AttackSpeed = 0.8 }
            };

            Weapon axe = new Weapon()
            {
                Name = "Common Axe",
                LevelRequired = 1,
                EquipmentSlot = EquipmentSlot.Weapon,
                WeaponType = WeaponType.Axe,
                WeaponAttributes = new WeaponAttributes() { Damage = 12, AttackSpeed = 0.8 }
            };

            Armor plateBody = new Armor()
            {
                Name = "Common plate body armor",
                LevelRequired = 1,
                EquipmentSlot = EquipmentSlot.Body,
                ArmorType = ArmorType.Plate,
                Attributes = new PrimaryAttributes() { Strength = 1 }
            };

            Armor clothHead = new Armor()
            {
                Name = "Common cloth head armor",
                LevelRequired = 1,
                EquipmentSlot = EquipmentSlot.Head,
                ArmorType = ArmorType.Cloth,
                Attributes = new PrimaryAttributes() { Strength = 1 }
            };

            equipmentList.Add(bow);
            equipmentList.Add(axe);
            equipmentList.Add(plateBody);
            equipmentList.Add(clothHead);

            for (int i = 1; i <= equipmentList.Count; i++)
            {
                Console.WriteLine(i + ". " + equipmentList[i-1].Name);
            }
            bool loopRunning = true;
            while (loopRunning)
            {
                string input = Console.ReadLine();
                try
                {
                    switch (input)
                    {
                        case "1":
                            character.EquipItem((Weapon)equipmentList[Int32.Parse(input)-1]);
                            break;
                        case "2":
                            character.EquipItem((Weapon)equipmentList[Int32.Parse(input)-1]);
                            break;
                        case "3":
                            character.EquipItem((Armor)equipmentList[Int32.Parse(input) - 1]);
                            break;
                        case "4":
                            character.EquipItem((Armor)equipmentList[Int32.Parse(input) - 1]);
                            break;
                        default:
                            Console.Write("Wrong input, going back to main menu");
                            break;
                    }
                    loopRunning = false;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.Write("Try inputting value again: ");
                }
            }
            
        }
    }
}
