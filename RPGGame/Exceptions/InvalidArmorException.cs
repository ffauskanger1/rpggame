﻿using RPGGame.ItemsAndEquipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGGame.Exceptions
{
    public  class InvalidArmorException : Exception
    {
        /// <summary>
        /// Empty constructor for Invalid Armor Exception
        /// </summary>
        public InvalidArmorException()
        {

        }

        /// <summary>
        /// Constructor for Invalid Armor Exception, so custom string can be set.
        /// </summary>
        /// <param name="message">The custom string</param>
        public InvalidArmorException(string message) : base(message)
        {
        }

        // Overriding the message property to set a default value
        public override string Message => "Armor cannot be equipped due to an error";
    }
}
