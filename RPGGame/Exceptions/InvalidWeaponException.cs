﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGGame.Exceptions
{
    public class InvalidWeaponException : Exception
    {
        /// <summary>
        /// Empty constructor for Invalid Weapon Exception
        /// </summary>
        public InvalidWeaponException()
        {

        }

        /// <summary>
        /// Constructor for Invalid Weapon Exception, so custom string can be set.
        /// </summary>
        /// <param name="message">The custom string</param>
        public InvalidWeaponException(string message) : base(message)
        {
        }

        // Overriding the message property to set a default value
        public override string Message => "Weapon cannot be equipped due to an error";
    }
}
